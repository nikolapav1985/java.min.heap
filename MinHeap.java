import java.util.*;

class MinHeap<T>{ // min heap
    public void insert(T[] arr,T item,Heap h,Comparator cmp){ // insert into min heap
        int pos;
        if(h.next==1){ // add first element to the heap
            arr[h.next]=item;
            h.next++;
            return;
        }
        pos=h.next;
        h.next++;
        for(;pos>1;pos=pos/2){ // add another element to the heap, start from bottom
            if(cmp.compare(item,arr[pos/2])<0){
                arr[pos]=arr[pos/2];
                continue;
            }
            break;
        }
        arr[pos]=item;
    }
    public T remove(T[] arr,Heap h,Comparator cmp){ // remove from min heap
        int pos=1;
        T rm=arr[pos];
        T min;
        arr[pos]=arr[h.next-1];
        h.next--;
        min=arr[pos];
        for(;2*pos<=h.next && (2*pos+1)<=h.next;){
            if(cmp.compare(min,arr[2*pos])<0 && cmp.compare(min,arr[2*pos+1])<0){
                break; // min heap property achieved
            }
            if(cmp.compare(arr[2*pos],arr[2*pos+1])<0){
                pos=2*pos; // left child is lesser
                arr[pos/2]=arr[pos];
                continue;
            }
            if(cmp.compare(arr[2*pos+1],arr[2*pos])<0){
                pos=2*pos+1; // right child is lesser
                arr[pos/2]=arr[pos];
            }
        }
        arr[pos]=min;
        return rm;
    }
}
