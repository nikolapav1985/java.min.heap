import java.util.*;

public class MaxTest{
    public static void main(String args[]){
        Scanner in=new Scanner(System.in);
        int count;
        int i;
        Integer item;
        Integer[] arr;
        Heap h=new Heap();
        MaxHeap<Integer> max=new MaxHeap<Integer>();
        Comparator cmp=new CompInt();

        count=Integer.parseInt(in.nextLine());
        System.out.println(String.format("count %d",count));
        arr=new Integer[2*count];
        for(i=0;i<count;i++){
            item=Integer.parseInt(in.nextLine());
            max.insert(arr,item,h,cmp);
        }
        for(i=0;i<2*count;i++){
            System.out.println(String.format("array item %d",arr[i]));
        }
        for(;h.next>1;){
            item=max.remove(arr,h,cmp);
            System.out.println(String.format("item %d",item));
        }
    }
}
