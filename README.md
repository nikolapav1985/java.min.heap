Java min heap implementation
----------------------------

- parent node found at position i (i=1,2,3...)
- left child found at position 2*i
- right child found at position 2*i+1
- implemented using array
- can insert element
- can remove element (in this case can be used for heapsort, as priority queue and so on)
- implemented max heap too

Compile
-------

- javac *.java

Test
----

- e.g. java MinTest < items
- e.g. java MaxTest < items

Test environment
----------------

- javac version 14.0.2
- java librca
- os lubuntu kernel version 4.13.0
